import 'dart:async';

import 'package:checkin/model/worked-duration.dart';
import 'package:checkin/service/database-provider.dart';

import 'bloc-provider.dart';


class WorkedDurationBloc implements BlocBase {
  static const String THIS_WEEK = 'thisWeek';
  static const String TODAY = 'today';
  static const String NOT_TODAY = 'notToday';
  static const String START_TIMER = 'startTimer';
  static const String END_TIMER = 'endTimer';

  static DateTime startDate;
  static DateTime endDate;

  // central
  final _workedDurationType = StreamController<String>.broadcast();
  StreamSink<String> get sinkWorkedDurationType => _workedDurationType.sink;
  Stream<String> get _streamWorkedDurationType => _workedDurationType.stream;

  final _todayWorkedDuration = StreamController<List<WorkedDuration>>.broadcast();
  StreamSink<List<WorkedDuration>> get _sinkTodayDuration => _todayWorkedDuration.sink;
  Stream<List<WorkedDuration>> get streamTodayDuration => _todayWorkedDuration.stream;

  final _thisWeekWorkedDuration = StreamController<List<WorkedDuration>>.broadcast();
  StreamSink<List<WorkedDuration>> get _sinkThisWeekDuration => _thisWeekWorkedDuration.sink;
  Stream<List<WorkedDuration>> get streamThisWeekDuration => _thisWeekWorkedDuration.stream;

  final _notTodayWorkedDuration = StreamController<List<WorkedDuration>>.broadcast();
  StreamSink<List<WorkedDuration>> get _sinkNotTodayDuration => _notTodayWorkedDuration.sink;
  Stream<List<WorkedDuration>> get streamNotTodayDuration => _notTodayWorkedDuration.stream;

  final _isRunningController = StreamController<bool>.broadcast();
  StreamSink<bool> get sinkIsRunning => _isRunningController.sink;
  Stream<bool> get streamIsRunning => _isRunningController.stream;

  final _sharedPreferencesController = StreamController<bool>.broadcast();
  StreamSink<bool> get sinkSharedPreferences => _sharedPreferencesController.sink;
  Stream<bool> get streamSharedPreferences => _sharedPreferencesController.stream;

  final _permissionController = StreamController<bool>.broadcast();
  StreamSink<bool> get sinkPermission => _permissionController.sink;
  Stream<bool> get streamPermission => _permissionController.stream;

  WorkedDurationBloc() {
    _streamWorkedDurationType.listen(_retrieveData);
    streamIsRunning.listen(_insertData);
  }

  void checkIsRunning() async {
    if (await DatabaseProvider.db.checkIsRunning())
      sinkIsRunning.add(true);
    else
      sinkIsRunning.add(false);
  }

  void _retrieveData(type) async {
    switch (type) {
      case THIS_WEEK:
        List<WorkedDuration> _thisWeekWorkedDurationList = await DatabaseProvider.db.getThisWeekWorkedDuration();
        _sinkThisWeekDuration.add(_thisWeekWorkedDurationList);
        break;
      case TODAY:
        List<WorkedDuration> _todayWorkedDurationList = await DatabaseProvider.db.getTodayWorkedDuration();
        _sinkTodayDuration.add(_todayWorkedDurationList);
        break;
      case NOT_TODAY:
        List<WorkedDuration> _notTodayWorkedDurationList = await DatabaseProvider.db.getAllWorkedDurationExceptToday(startDate, endDate);
        _sinkNotTodayDuration.add(_notTodayWorkedDurationList);
        break;
      default:
        break;
    }
  }

  void _insertData(bool isStart) async {
    if (isStart) {
      if (await DatabaseProvider.db.checkIsRunning()) return;
      await DatabaseProvider.db.startDuration();
      sinkIsRunning.add(true);
    } else {
      if (!await DatabaseProvider.db.checkIsRunning()) return;
      await DatabaseProvider.db.endDuration();
      sinkIsRunning.add(false);
    }
    _retrieveData(TODAY);
    _retrieveData(THIS_WEEK);
  }

  @override
  void dispose() {
    _workedDurationType.close();
    _todayWorkedDuration.close();
    _thisWeekWorkedDuration.close();
    _notTodayWorkedDuration.close();
    _isRunningController.close();
    _sharedPreferencesController.close();
    _permissionController.close();
  }
}
