import 'package:checkin/helper/constant.dart';
import 'package:checkin/service/sharedpreferences-provider.dart';
import 'package:date_format/date_format.dart';

String getReadableTime(Duration duration) {
  if (duration == null) return '';
  String hrs = duration.inHours.toString().padLeft(2, '0');
  String min = duration.inMinutes.remainder(60).toString().padLeft(2, '0');
  String sec = duration.inSeconds.remainder(60).toString().padLeft(2, '0');

  return '$hrs:$min:$sec';
}

String getSQLDate(DateTime dateTime, { divider: '-' }) {
  if(dateTime == null) return '';
  return '${dateTime.year}$divider${dateTime.month.toString().padLeft(2, '0')}$divider${dateTime.day.toString().padLeft(2, '0')}';
}

String getReadableDate(DateTime dateTime, { showYear: true }) {
  if(dateTime == null) return '';
  List<String> format = [dd, ' ', M, ' '];
  if(showYear) { format.add(' '); format.add(yyyy); }
  return '${formatDate(dateTime, format)}';
}

String getDayName(DateTime dateTime) {
  if(dateTime == null) return '';
  return '${formatDate(dateTime, [D])} (${getReadableDate(dateTime, showYear: false)})';
}

String getReadableHourMinute(Duration duration) {
  if(duration == null) return '';
  String hrs = duration.inHours.toString().padLeft(2, '0');
  String min = duration.inMinutes.remainder(60).toString().padLeft(2, '0');

  return '${hrs}h ${min}m';
}

String getHours(Duration duration) {
  if(duration == null) return '--';
  return duration.inHours.toString().padLeft(2, '0');
}

String getMinutes(Duration duration) {
  if(duration == null) return '--';
  return duration.inMinutes.remainder(60).toString().padLeft(2, '0');
}

String getSeconds(Duration duration) {
  if(duration == null) return '--';
  return duration.inSeconds.remainder(60).toString().padLeft(2, '0');
}

bool isSameDate(DateTime date1, DateTime date2){
  return (date1.year == date2.year && date1.month == date2.month && date1.day == date2.day);
}

int isoWeekNumber(DateTime date) {
  int daysToAdd = DateTime.thursday - date.weekday;
  DateTime thursdayDate = daysToAdd > 0 ? date.add(Duration(days: daysToAdd)) : date.subtract(Duration(days: daysToAdd.abs()));
  int dayOfYearThursday = dayOfYear(thursdayDate);
  return 1 + ((dayOfYearThursday - 1) / 7).floor();
}

int dayOfYear(DateTime date) {
  return date.difference(DateTime(date.year, 1, 1)).inDays;
}

String getDateDuration(DateTime startDate, DateTime endDate) {
  return '${formatDate(startDate, [dd, ' ', M])} - ${formatDate(endDate, [dd, ' ', M, ' ', yyyy])}';
}

String getTimeDuration(DateTime startTime, DateTime endTime) {
  return '${formatDate(startTime, [HH, ':', nn, ':', ss, ' ', am])} - ${formatDate(endTime, [HH, ':', nn, ':', ss, ' ', am])}';
}

Duration getWeeklyRequiredDuration() {
  int workingDays = SharedPreferencesProvider.get(WORKING_DAYS);
  return new Duration(minutes: (SharedPreferencesProvider.get(REQUIRED_MINUTES_DAILY) ?? 0) * workingDays);
}

Duration getDailyRequiredDuration() {
  return new Duration(minutes: (SharedPreferencesProvider.get(REQUIRED_MINUTES_DAILY) ?? 0));
}

DateTime getFirstDateOfWeek(DateTime dateTime) {
  DateTime dateToCalc = dateTime;
  if(dateTime.weekday == 1) dateToCalc = dateTime.subtract(Duration(days: 1));
  DateTime startDate = dateToCalc.subtract(Duration(days: dateToCalc.weekday - 1));
  return startDate;
}

DateTime getLastDateOfWeek(DateTime dateTime) {
  DateTime endDate = dateTime.add(Duration(days: DateTime.daysPerWeek - dateTime.weekday));
  return endDate;
}
