import 'dart:ui';

import 'package:flutter/material.dart';

Color getProgressColor(progress, {swatch: 500}) {
  if(progress == null) return Colors.red[swatch];
  if (progress >= 0 && progress <= 25) {
    return Colors.red[swatch];
  }
  if (progress > 25 && progress <= 50) {
    return Colors.orange[swatch];
  }
  if (progress > 50 && progress <= 75) {
    return Colors.lime[swatch];
  }
  return Colors.green[swatch];
}