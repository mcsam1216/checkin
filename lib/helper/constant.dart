library constants;

const String REQUIRED_MINUTES_DAILY = 'requiredWorkingMinutes';
const String DESIRE_NETWORK = 'desiredNetwork';
const String WORKING_DAYS = 'workingDays';
const int LOADING_TIME = 1500;
const double NAVBAR_HEIGHT = 60.0;

const BUY_ME_A_COFFEE = 'https://www.buymeacoffee.com/oh8yMYq';
