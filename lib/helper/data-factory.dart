import 'package:checkin/helper/datetime.dart';
import 'package:checkin/model/worked-duration.dart';

List<WorkedDurationByWeek> groupByWeek(List<WorkedDuration> workedDurations) {
  List<WorkedDurationByWeek> workedDurationsByWeek = [];
  groupByDay(workedDurations).forEach((workedDuration) {
    int existsIndex = workedDurationsByWeek.indexWhere((workedDurationByWeek) => isoWeekNumber(workedDurationByWeek.startDate) == isoWeekNumber(workedDuration.startTime));

    if(existsIndex >= 0){
      WorkedDurationByWeek _currentWorkedDurationByWeek = workedDurationsByWeek[existsIndex];

      if(workedDuration.startTime.isBefore(_currentWorkedDurationByWeek.startDate)) _currentWorkedDurationByWeek.startDate = workedDuration.startTime;
      if(workedDuration.endTime.isAfter(_currentWorkedDurationByWeek.endDate)) _currentWorkedDurationByWeek.endDate = workedDuration.endTime;

      _currentWorkedDurationByWeek.totalWorkedDuration += workedDuration.endTime.difference(workedDuration.startTime);

      List<WorkedDurationByDay> _currentWorkedDurationByDay = _currentWorkedDurationByWeek.dailyWorkedDuration;
      _currentWorkedDurationByWeek.dailyWorkedDuration = [..._currentWorkedDurationByDay, ...[workedDuration]];
    } else {
      workedDurationsByWeek.add(new WorkedDurationByWeek(
        startDate: workedDuration.startTime,
        endDate: workedDuration.endTime,
        totalWorkedDuration: workedDuration.endTime.difference(workedDuration.startTime),
        dailyWorkedDuration: [workedDuration]
      ));
    }
  });

  return workedDurationsByWeek;
}

List<WorkedDurationByDay> groupByDay(List<WorkedDuration> workedDurations) {
  List<WorkedDurationByDay> workedDurationsByDay = [];
  workedDurations.forEach((workedDuration) {
    int existsIndex = workedDurationsByDay.indexWhere((workedDurationByDay) => isSameDate(workedDurationByDay.startTime, workedDuration.startTime));

    if(existsIndex >= 0) {
      WorkedDurationByDay _currentWorkedDurationByDay = workedDurationsByDay[existsIndex];

      if(workedDuration.startTime.isBefore(_currentWorkedDurationByDay.startTime)) _currentWorkedDurationByDay.startTime = workedDuration.startTime;
      if(workedDuration.endTime.isAfter(_currentWorkedDurationByDay.endTime)) _currentWorkedDurationByDay.endTime = workedDuration.endTime;

      _currentWorkedDurationByDay.totalWorkedDuration += workedDuration.endTime.difference(workedDuration.startTime);
    } else
      workedDurationsByDay.add(new WorkedDurationByDay(startTime: workedDuration.startTime, endTime: workedDuration.endTime, totalWorkedDuration: workedDuration.endTime.difference(workedDuration.startTime)));
  });
  return workedDurationsByDay;
}