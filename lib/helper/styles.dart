import 'package:flutter/material.dart';

ThemeData mainTheme = ThemeData(
  primarySwatch: Colors.orange,
  primaryColor: Colors.orange,
  accentColor: Colors.orangeAccent,
  backgroundColor: Colors.orangeAccent,
  accentColorBrightness: Brightness.light,
  splashColor: Colors.orangeAccent[100],
  highlightColor: Colors.transparent,
  dividerColor: Colors.grey[400],
  fontFamily: 'Roboto',
  textTheme: TextTheme(
    body1: TextStyle(fontFamily: 'Roboto', fontSize: 16.0, color: Colors.black54),
    title: TextStyle(fontFamily: 'Lato', fontSize: 36.0, color: Colors.black54),
    caption: TextStyle(fontFamily: 'Roboto', fontSize: 20.0, color: Colors.black54),
    subtitle: TextStyle(fontFamily: 'Lato', fontSize: 24.0, color: Colors.black54),
  )
);

ThemeData datePickerTheme = ThemeData.light().copyWith(
  primaryColor: Colors.orange,
  accentColor: Colors.orangeAccent,
  colorScheme: ColorScheme.light(primary: Colors.orange),
  buttonTheme: ButtonThemeData(
      textTheme: ButtonTextTheme.primary
  ),
);

BoxShadow checkinBoxShadow = BoxShadow(
  color: Colors.grey[300],
  blurRadius: 5.0,
  offset: Offset(
    0,
    0.75, // vertical, move down 10
  ),
);