import 'dart:async';

import 'package:checkin/widget/empty.dart';
import 'package:checkin/widget/error.dart';
import 'package:flutter/cupertino.dart';

class CheckinStreamBuilder extends StatefulWidget {
  CheckinStreamBuilder({@required this.stream, @required this.streamWidget, this.loader});

  final Stream<List> stream;
  final Function streamWidget;
  final Widget loader;

  @override
  _CheckinStreamBuilderState createState() => _CheckinStreamBuilderState();
}

class _CheckinStreamBuilderState extends State<CheckinStreamBuilder> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: widget.stream,
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        if (snapshot.hasError)
          return SliverToBoxAdapter(child: Error());
        switch (snapshot.connectionState) {
          case ConnectionState.none: return SliverToBoxAdapter(child: Error());
          case ConnectionState.waiting: return SliverToBoxAdapter(child: widget.loader ?? Text('Loading...'));
          case ConnectionState.active: {
            if(!snapshot.hasData || snapshot.data.isEmpty) return SliverToBoxAdapter(child: Empty());

            return widget.streamWidget(snapshot);
          }
          case ConnectionState.done: return SliverToBoxAdapter(child: Text('\$${snapshot.data} (closed)'));
        }
        return null; // unreachable
      },
    );
  }
}
