import 'package:flutter/cupertino.dart';

class CheckinSliverScrollView extends StatefulWidget {
  CheckinSliverScrollView({@required this.children, this.scrollController, this.ignoreStatusBar});

  final List<Widget> children;
  final ScrollController scrollController;
  final bool ignoreStatusBar;

  @override
  _CheckinSliverScrollViewState createState() => _CheckinSliverScrollViewState();
}

class _CheckinSliverScrollViewState extends State<CheckinSliverScrollView> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      controller: widget.scrollController,
      slivers: <Widget>[
        widget.ignoreStatusBar ?? false ? SliverToBoxAdapter() : SliverPadding(
          padding: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
        ),
        for(Widget child in widget.children) child,
      ],
    );
  }
}
