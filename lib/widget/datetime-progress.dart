import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

TextStyle timeStyle(context) => Theme.of(context).textTheme.caption.copyWith(fontWeight: FontWeight.bold);

class DateTimeProgress extends StatelessWidget {
  DateTimeProgress({@required this.workedDuration, @required this.requiredWorkedDuration, this.showMinutes: false});

  final Duration workedDuration;
  final Duration requiredWorkedDuration;
  final bool showMinutes;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Align(
          alignment: Alignment.topLeft,
          child: Wrap(
            direction: Axis.horizontal,
            children: <Widget>[
              Text(workedDuration.inHours.toString(), style: timeStyle(context),),
              Text('hrs'),
              showMinutes ? Text(workedDuration.inMinutes.remainder(60).toString(), style: timeStyle(context),) : Container(),
              showMinutes ? Text('min',) : Container(),
            ],
          ),
        ),
        Align(alignment: Alignment.center, child: Text('/', style: TextStyle(fontSize: 45.0,),)),
        Align(
          alignment: Alignment.bottomRight,
          child: Wrap(
            direction: Axis.horizontal,
            children: <Widget>[
              Text(requiredWorkedDuration.inHours.toString(), style: Theme.of(context).textTheme.caption.copyWith(fontWeight: FontWeight.bold)),
              Text('hrs'),
              showMinutes ? Text(requiredWorkedDuration.inMinutes.remainder(60).toString(), style: timeStyle(context),) : Container(),
              showMinutes ? Text('min',) : Container(),
            ],
          ),
        ),
      ],);
  }
}
