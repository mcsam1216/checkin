import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Empty extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(45.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Image(
              image: AssetImage('assets/images/empty.png'),
              width: MediaQuery.of(context).size.width * 0.6,
            ),
            SizedBox(height: 25.0,),
            Text('No records yet', style: Theme.of(context).textTheme.subtitle.copyWith(fontSize: 20.0, color: Colors.grey)),
          ],
        ),
      ),
    );
  }
}
