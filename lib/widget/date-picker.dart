import 'package:checkin/helper/datetime.dart';
import 'package:checkin/helper/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CheckinDatePicker extends StatelessWidget {
  CheckinDatePicker({this.context, this.dateTime, this.datePicked});

  final BuildContext context;
  final Function datePicked;
  final DateTime dateTime;

  Future<void> _showDatePicker() async {
    final DateTime pickedDateTime = await showDatePicker(
        context: context,
        initialDate: dateTime,
        firstDate: new DateTime.now().subtract(new Duration(days: 90)),
        lastDate: new DateTime.now().add(new Duration(days: 15)),
        builder: (BuildContext context, Widget child) {
          return Theme(
            data: datePickerTheme,
            child: child,
          );
        }
    );

    if(pickedDateTime == null) {
      return new DateTime.now();
    }

    datePicked(pickedDateTime);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15.0),
      decoration: BoxDecoration(
        border: Border.all(color: Theme.of(context).dividerColor),
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: InkWell(
        onTap: _showDatePicker,
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(
                Icons.date_range,
                color: Theme.of(context).dividerColor,
                size: 14.0,
              ),
              SizedBox(width: 5.0,),
              Text('${getReadableDate(dateTime)}', style: TextStyle(color: Colors.grey, fontSize: 16.0),),
            ],
          ),
        ),
      ),
    );
  }
}
