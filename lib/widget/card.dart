import 'dart:async';

import 'package:checkin/helper/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CheckinCard extends StatefulWidget {
  CheckinCard({@required this.child, this.margin, this.shadow});

  final Widget child;
  final EdgeInsets margin;
  final bool shadow;

  @override
  _CheckinCardState createState() => _CheckinCardState();
}

class _CheckinCardState extends State<CheckinCard> {
  bool _fadeIn = false;
  Timer _timer;

  void _fadeInCard(i) {
    _timer.cancel();
    setState(() => _fadeIn = true);
  }

  @override
  void initState() {
    super.initState();
    _timer = Timer.periodic(Duration(milliseconds: 300), _fadeInCard);
  }


  @override
  void dispose() {
    super.dispose();
    if(_timer != null) _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      duration: Duration(milliseconds: 300),
      opacity: _fadeIn ? 1 : 0,
      child: Container(
        margin: widget.margin,
        padding: EdgeInsets.all(20.0),
        decoration: BoxDecoration(
            boxShadow: widget.shadow ?? true ? [
              checkinBoxShadow
            ] : null,
            borderRadius: BorderRadius.circular(15.0),
            color: Colors.white
        ),
        child: widget.child,
      ),
    );
  }
}
