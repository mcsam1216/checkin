import 'package:flutter/cupertino.dart';

class CheckinSliverList extends StatefulWidget {
  CheckinSliverList({
    @required this.items,
    @required this.itemBuilder
  });

  final List<dynamic> items;
  final Function itemBuilder;

  @override
  _CheckinSliverListState createState() => _CheckinSliverListState();
}

class _CheckinSliverListState extends State<CheckinSliverList> {
  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildBuilderDelegate(widget.itemBuilder),
    );
  }
}
