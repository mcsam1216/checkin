import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class CheckinLoader extends StatefulWidget {
  CheckinLoader({@required this.child});

  final Widget child;

  @override
  _CheckinLoaderState createState() => _CheckinLoaderState();
}

class _CheckinLoaderState extends State<CheckinLoader> {
  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(child: widget.child, baseColor: Colors.grey[200], highlightColor: Colors.white);
  }
}
