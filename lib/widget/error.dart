import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Error extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image(
            image: AssetImage('assets/images/error.png'),
          ),
          SizedBox(height: 25.0,),
          Text('Opps, Something went wrong', style: Theme.of(context).textTheme.subtitle.copyWith(fontSize: 20.0, color: Colors.grey)),
        ],
      ),
    );
  }
}
