import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void showSnackBar(BuildContext context, String text) => Scaffold.of(context).showSnackBar(new SnackBar(content: new Text(text), backgroundColor: Theme.of(context).accentColor,));
