import 'package:checkin/bloc/worked-duration-bloc.dart';
import 'package:checkin/helper/datetime.dart';
import 'package:checkin/helper/progress.dart';
import 'package:checkin/model/worked-duration.dart';
import 'package:checkin/widget/card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';


class DurationItemByWeek extends StatefulWidget {
  DurationItemByWeek({
    @required this.item,
    @required this.bloc
  });

  final WorkedDurationBloc bloc;
  final WorkedDurationByWeek item;
  @override
  _DurationItemByWeekState createState() => _DurationItemByWeekState();
}

class _DurationItemByWeekState extends State<DurationItemByWeek> {
  Duration _requiredWeeklyWorkedDuration;

  void _showBottomSheet() {
    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ),
        backgroundColor: Colors.white,
        context: context,
        builder: (BuildContext context) {
          List<WorkedDurationByDay> _workedDurationsByDay = widget.item.dailyWorkedDuration;
          _workedDurationsByDay.sort((curr, next) => curr.startTime.compareTo(next.startTime));
          int longestMinutesWorked = (_workedDurationsByDay.reduce((curr, next) => curr.totalWorkedDuration.inMinutes > next.totalWorkedDuration.inMinutes ? curr : next)).totalWorkedDuration.inMinutes;

          return Container(
            height: MediaQuery.of(context).size.height * 0.2 + (75 * _workedDurationsByDay.length),
            padding: EdgeInsets.all(25.0),
              decoration: BoxDecoration(color: Colors.white, borderRadius: new BorderRadius.only(
                  topLeft: const Radius.circular(25.0),
                  topRight: const Radius.circular(25.0))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 35.0),
                  child: Text(getDateDuration(widget.item.startDate, widget.item.endDate), style: Theme.of(context).textTheme.subtitle,),
                ),
                Expanded(
                  child: ListView.builder(
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: _workedDurationsByDay.length,
                      itemBuilder: (BuildContext context, int index) {
                        WorkedDurationByDay _workedDurationByDay = _workedDurationsByDay[index];
                        double _widthRatio = _workedDurationByDay.totalWorkedDuration.inMinutes / longestMinutesWorked;
                        bool _isLong = _widthRatio > 0.5;

                        return Padding(
                          padding: const EdgeInsets.symmetric(vertical: 12.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('${getDayName(_workedDurationByDay.startTime)}'),
                              SizedBox(height: 5.0,),
                              Row(
                                children: <Widget>[
                                  Container(
                                    decoration: BoxDecoration(borderRadius: BorderRadius.only(topRight: Radius.circular(8.0), bottomRight: Radius.circular(8.0)), color: Theme.of(context).accentColor),
                                    margin: _isLong ? null : EdgeInsets.only(right: 5.0),
                                    padding: _isLong ? EdgeInsets.only(right: 8.0) : null,
                                    width: MediaQuery.of(context).size.width * 0.85 * _widthRatio,
                                    height: 25.0,
                                    child: _isLong ? Align(alignment: Alignment.centerRight, child: Text('${getReadableHourMinute(_workedDurationByDay.totalWorkedDuration)}', style: TextStyle(color: Colors.white),),) : null,
                                  ),
                                  (_isLong ? Container() : Text('${getReadableHourMinute(_workedDurationByDay.totalWorkedDuration)}', style: Theme.of(context).textTheme.body1,))
                                ],
                              ),
                            ],
                          ),
                        );
                      }),
                ),
              ],
            ),
          );
        });
  }

  @override
  void initState() {
    super.initState();

    _requiredWeeklyWorkedDuration = getWeeklyRequiredDuration();
  }

  @override
  Widget build(BuildContext context) {
    int percentage = _requiredWeeklyWorkedDuration != null ? (((widget.item.totalWorkedDuration.inMinutes / _requiredWeeklyWorkedDuration.inMinutes) * 100).round()) : 0;

    return CheckinCard(
      margin: EdgeInsets.symmetric(vertical: 8.0, horizontal: 15.0),
      child: InkWell(
        onTap: _showBottomSheet,
        child: Stack(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                  Text(getDateDuration(widget.item.startDate, widget.item.endDate), style: Theme.of(context).textTheme.body1.merge(TextStyle(fontWeight: FontWeight.bold)),),
                  SizedBox(height: 25.0,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text('Weekly Progress'),
                      SizedBox(height: 10.0,),
                      Container(
                        height: 20.0,
                        width: 300.0,
                        child: FAProgressBar(
                          progressColor: getProgressColor(percentage),
                          currentValue: percentage > 100 ? 100 : percentage,
                          displayText: '%',
                        ),
                      ),
                    ],
                  ),
              ],
            ),
            Container(
              alignment: Alignment.topRight,
              child: RichText(text: TextSpan(text: '${widget.item.totalWorkedDuration.inHours.toString()}h', style: Theme.of(context).textTheme.subtitle.merge(TextStyle(fontSize: 30.0)), children: <TextSpan>[
                TextSpan(text: ' ${widget.item.totalWorkedDuration.inMinutes.remainder(60).toString().padLeft(2, '0')}m', style: Theme.of(context).textTheme.subtitle.merge(TextStyle(fontSize: 20.0))),
              ]),),
            )
          ],
        ),
      ),
    );
  }
}

