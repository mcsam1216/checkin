

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wifi/wifi.dart';
import 'package:checkin/widget/error.dart';
import 'package:checkin/widget/empty.dart';


class WifiListDialog extends StatefulWidget {
  @override
  _WifiListDialogState createState() => _WifiListDialogState();
}

class _WifiListDialogState extends State<WifiListDialog> {
  String _selectedWifi;

  Future<List<WifiResult>> _getWifiList() async {
    List<WifiResult> result = await Wifi.list('');

    return result;
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(15.0))),
      content: Container(
        height: MediaQuery.of(context).size.height * 0.7,
        width: MediaQuery.of(context).size.width * 0.8,
        child: FutureBuilder(
          future: _getWifiList(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if(snapshot.hasError) return Error();
            switch(snapshot.connectionState) {
              case ConnectionState.none: return Error();
              case ConnectionState.waiting:
              case ConnectionState.active: return Center(child: CircularProgressIndicator(backgroundColor: Colors.white,));
              case ConnectionState.done: {

                if(!snapshot.hasData || snapshot.data.isEmpty) return Empty();

                List<dynamic> _wifiList = snapshot.data.map((wifi) => wifi.ssid.toString()).where((wifi) => wifi != '').toList().toSet().toList();

                return ListView.builder(
                    itemCount: _wifiList?.length ?? 0,
                    itemBuilder: (BuildContext context, int index) {
                      String _wifiSSID = _wifiList.elementAt(index);
                      bool _isSelected = _selectedWifi != null && _selectedWifi == _wifiSSID;

                      return InkWell(
                        onTap: () => setState(() => _selectedWifi = _wifiSSID),
                        child: IgnorePointer(
                          child: Container(
                            padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 15.0),
                            decoration: BoxDecoration(
                              border: Border(bottom: BorderSide(width: 1.0, color: Theme.of(context).dividerColor)),
                              color: _isSelected ? Colors.grey[400] : Colors.white,
                            ),
                            child: Container(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(_wifiSSID, style: TextStyle(fontSize: 14.0, color: _isSelected ? Colors.white : Colors.black54),),
                                  Icon((_isSelected ? Icons.check : null), color: Colors.white),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    });
              }
            }
            return null;
          },
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('CANCEL'),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        FlatButton(
          child: Text('OK'),
          onPressed: () {
            Navigator.of(context).pop(_selectedWifi);
          },
        )
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    _getWifiList();
  }
}

