import 'package:checkin/helper/datetime.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'card.dart';

class TodayItem extends StatefulWidget {
  TodayItem({this.startTime, this.endTime});

  final DateTime startTime;
  final DateTime endTime;

  @override
  _TodayItemState createState() => _TodayItemState();
}

class _TodayItemState extends State<TodayItem> {

  TextStyle captionStyle(context) => Theme.of(context).textTheme.caption.copyWith(fontSize: 24.0);

  @override
  Widget build(BuildContext context) {
    return CheckinCard(
      shadow: widget.startTime != null &&  widget.endTime != null,
      margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Wrap(
            direction: Axis.horizontal,
            children: <Widget>[
              Wrap(
                direction: Axis.horizontal,
                children: <Widget>[
                  Text(widget.startTime?.hour?.toString()?.padLeft(2, '0') ?? '', style:  captionStyle(context),),
                  SizedBox(width: 2.0,),
                  Text(widget.startTime?.minute?.toString()?.padLeft(2, '0') ?? ''),
                ],
              ),
              Text(' - '),
              Wrap(
                direction: Axis.horizontal,
                children: <Widget>[
                  Text(widget.endTime?.hour?.toString()?.padLeft(2, '0') ?? '', style: captionStyle(context),),
                  SizedBox(width: 2.0,),
                  Text(widget.endTime?.minute?.toString()?.padLeft(2, '0') ?? ''),
                ],
              ),
            ],
          ),
          Wrap(
            direction: Axis.horizontal,
            children: <Widget>[
              Text(widget.endTime?.difference(widget.startTime)?.inHours?.toString() ?? '', style: captionStyle(context),),
              Text('hrs'),
              SizedBox(width: 5.0,),
              Text(widget.endTime?.difference(widget.startTime)?.inMinutes?.remainder(60)?.toString() ?? '', style: captionStyle(context),),
              Text('min'),
            ],
          )
        ],
      ),
    );
  }
}
