import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TextTitle extends StatefulWidget {
  TextTitle({@required this.title});

  final String title;

  @override
  _TextTitleState createState() => _TextTitleState();
}

class _TextTitleState extends State<TextTitle> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 35.0, vertical: 15.0),
      child: Text(widget.title, style: Theme.of(context).textTheme.title,),
    );
  }
}
