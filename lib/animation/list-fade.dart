import 'package:checkin/helper/constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:simple_animations/simple_animations/controlled_animation.dart';
import 'package:simple_animations/simple_animations/multi_track_tween.dart';

class FadeIn extends StatelessWidget {
  FadeIn({this.delay, this.child});

  final double delay;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    int animation_duration = ((10-delay) * 100).round();
    final tween = MultiTrackTween([
      Track("opacity")
          .add(Duration(milliseconds: animation_duration), Tween(begin: 0.0, end: 1.0)),
      Track("width").add(
          Duration(milliseconds: animation_duration), Tween(begin: 0.0, end: 380.0),
          curve: Curves.easeOut)
    ]);

    return ControlledAnimation(
      playback: Playback.MIRROR,
      delay: Duration(milliseconds: (100 * delay).round()),
      duration: tween.duration,
      tween: tween,
      child: child,
      builderWithChild: (context, child, animation) => Opacity(
        opacity: animation["opacity"],
        child: Container(
            width: animation["width"], child: child),
      ),
    );
  }
}