import 'package:checkin/helper/constant.dart';
import 'package:checkin/service/sharedpreferences-provider.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';

import 'database-provider.dart';

Future<void> streamConnectivity() async {
  WidgetsFlutterBinding.ensureInitialized();

  await SharedPreferencesProvider.initialize();

  bool isConnectedToDesired = await DatabaseProvider.db.checkIsRunning();

  Connectivity connectivity = Connectivity();
  connectivity.onConnectivityChanged.listen((connectivityType) async {
    print('Listening on background');
    String desiredSSID = SharedPreferencesProvider.get(DESIRE_NETWORK) ?? '';
    String wifiSSID = await connectivity.getWifiName();

    if (connectivityType == ConnectivityResult.wifi && wifiSSID == desiredSSID && !isConnectedToDesired) {
      DatabaseProvider.db.startDuration();
      isConnectedToDesired = true;
    }

    if (connectivityType == ConnectivityResult.none && isConnectedToDesired) {
      DatabaseProvider.db.endDuration();
      isConnectedToDesired = false;
    }
  });
}
