import 'dart:io';

import 'package:checkin/helper/datetime.dart';
import 'package:checkin/model/worked-duration.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider {
  DatabaseProvider._();

  static final DatabaseProvider db = DatabaseProvider._();
  Database _database;

  Future<void> insertDummy() async {
    _database.delete(WorkedDuration.TABLE_NAME);
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 4, 7, 9, 11, 01), endTime: DateTime(2020, 4, 7, 18, 42, 01)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 4, 8, 9, 11, 01), endTime: DateTime(2020, 4, 8, 18, 42, 01)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 4, 9, 9, 11, 01), endTime: DateTime(2020, 4, 9, 18, 42, 01)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 4, 10, 9, 11, 01), endTime: DateTime(2020, 4, 10, 18, 42, 01)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 4, 11, 10, 31, 01), endTime: DateTime(2020, 4, 11, 19, 51, 01)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 4, 12, 10, 31, 01), endTime: DateTime(2020, 4, 12, 19, 51, 01)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 4, 13, 10, 31, 01), endTime: DateTime(2020, 4, 13, 19, 51, 01)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 4, 14, 9, 31, 01), endTime: DateTime(2020, 4, 14, 20, 14, 01)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 4, 1, 10, 31, 01), endTime: DateTime(2020, 4, 1, 19, 51, 01)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 4, 2, 10, 31, 01), endTime: DateTime(2020, 4, 2, 19, 51, 01)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 3, 11, 10, 31, 01), endTime: DateTime(2020, 3, 11, 19, 51, 01)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 3, 12, 9, 43, 01), endTime: DateTime(2020, 3, 12, 21, 09, 11)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 3, 13, 9, 43, 01), endTime: DateTime(2020, 3, 13, 21, 09, 11)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 3, 1, 9, 31, 01), endTime: DateTime(2020, 3, 1, 15, 46, 23)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 3, 2, 11, 51, 12), endTime: DateTime(2020, 3, 2, 20, 34, 43)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 3, 3, 10, 43, 45), endTime: DateTime(2020, 3, 3, 20, 55, 21)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 3, 4, 9, 12, 45), endTime: DateTime(2020, 3, 4, 17, 53, 24)).toMap());
    _database.insert(WorkedDuration.TABLE_NAME,
        new WorkedDuration(startTime: DateTime(2020, 3, 5, 10, 43, 45), endTime: DateTime(2020, 3, 5, 20, 55, 21)).toMap());
    _database.rawInsert('INSERT into ${WorkedDuration.TABLE_NAME}(startTime, endTime) VALUES(\'1586625321000\', \'0\')');
  }

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await getDatabaseInstance();
//    print(await _database
//        .rawQuery('''
//        SELECT *
//        FROM ${WorkedDuration.TABLE_NAME}
//        ORDER BY startTime DESC
//        '''));
//    await insertDummy();
    return _database;
  }

  Future<Database> getDatabaseInstance() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, "person.db");
    return await openDatabase(path, version: 1, onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE ${WorkedDuration.TABLE_NAME} ("
          "id integer primary key AUTOINCREMENT,"
          "startTime integer,"
          "endTime integer"
          ")");
    });
  }

  Future<bool> checkIsRunning() async {
    final db = await database;
    List<Map> result = await db.rawQuery('''
            SELECT * 
            FROM ${WorkedDuration.TABLE_NAME} 
            WHERE strftime(\'%d-%m-%Y\', startTime / 1000, \'unixepoch\', \'localtime\') = strftime(\'%d-%m-%Y\', date(\'now\', \'localtime\')) 
            AND endTime = 0
            ''');
    if (result.length == 0) return false;
    return result.first.isNotEmpty;
  }

  Future<void> startDuration() async {
    if (await checkIsRunning()) return null;
    final db = await database;
    WorkedDuration startDuration = new WorkedDuration(startTime: DateTime.now(), endTime: null);

    await db.insert(
      WorkedDuration.TABLE_NAME,
      startDuration.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    print('Starting timer...');
  }

  Future<void> endDuration() async {
    if (!await checkIsRunning()) return null;
    final db = await database;
    List<Map> map = await db.query(WorkedDuration.TABLE_NAME, where: 'endTime = ?', whereArgs: [0]);
    if (map.isEmpty) return 0;

    WorkedDuration workedDuration = WorkedDuration.fromMap(map.last);
    DateTime _now = DateTime.now();
    DateTime _startDate = workedDuration.startTime;

    int _dayDifferences = DateTime(_now.year, _now.month, _now.day).difference(DateTime(_startDate.year, _startDate.month, _startDate.day)).inDays;
    if(_dayDifferences > 0) {
      for(int i=0; i <= _dayDifferences; i ++) {
        if(i == 0) {
          workedDuration.endTime = DateTime(_startDate.year, _startDate.month, _startDate.day, 23, 59, 59);
          db.update(WorkedDuration.TABLE_NAME, workedDuration.toMap(), where: 'id = ?', whereArgs:  [workedDuration.id]);
        }

        if(i > 0 && i < _dayDifferences) {
          DateTime _dateInMiddle = _startDate.add(Duration(days: i));
          db.insert(WorkedDuration.TABLE_NAME,
              new WorkedDuration(startTime: DateTime(_dateInMiddle.year, _dateInMiddle.month, _dateInMiddle.day, 0, 0, 0), endTime: DateTime(_dateInMiddle.year, _dateInMiddle.month, _dateInMiddle.day, 23, 59, 59)).toMap());
        }

        if(i == _dayDifferences)
          db.insert(WorkedDuration.TABLE_NAME,
              new WorkedDuration(startTime: DateTime(_now.year, _now.month, _now.day, 0, 0, 0), endTime: _now).toMap());

      }
    } else {
      workedDuration.endTime = DateTime.now();
      await db.update(WorkedDuration.TABLE_NAME, workedDuration.toMap(), where: 'id = ?', whereArgs: [workedDuration.id]);
    }

    print('Ending timer...');
  }

  Future<List<WorkedDuration>> getAllWorkedDuration() async {
    final db = await database;
    return (await db.query(WorkedDuration.TABLE_NAME)).map<WorkedDuration>((workedDuration) => WorkedDuration.fromMap(workedDuration)).toList();
  }

  Future<List<WorkedDuration>> getTodayWorkedDuration() async {
    final db = await database;
    return (await db.rawQuery('''
            SELECT * 
            FROM ${WorkedDuration.TABLE_NAME} 
            WHERE strftime(\'%d-%m-%Y\', startTime / 1000, \'unixepoch\', \'localtime\') = strftime(\'%d-%m-%Y\', date(\'now\', \'localtime\')) 
            ORDER BY startTime DESC
            ''')).map<WorkedDuration>((workedDuration) => WorkedDuration.fromMap(workedDuration)).toList();
  }

  Future<List<WorkedDuration>> getThisWeekWorkedDuration() async {
    final db = await database;
    return (await db.rawQuery('''
            SELECT * 
            FROM ${WorkedDuration.TABLE_NAME} 
            WHERE strftime(\'%W-%Y\', startTime / 1000, \'unixepoch\', \'localtime\') = strftime(\'%W-%Y\', date(\'now\', \'localtime\')) 
            ORDER BY startTime DESC
            ''')).map<WorkedDuration>((workedDuration) => WorkedDuration.fromMap(workedDuration)).toList();
  }

  Future<List<WorkedDuration>> getAllWorkedDurationExceptToday(DateTime startDate, DateTime endDate) async {
    final db = await database;
    return (await db.rawQuery('''
            SELECT * 
            FROM ${WorkedDuration.TABLE_NAME} 
            WHERE strftime(\'%Y-%m-%d\', startTime / 1000, \'unixepoch\', \'localtime\') BETWEEN \'${getSQLDate(startDate)}\' AND \'${getSQLDate(endDate)}\'
            AND strftime(\'%d-%m-%Y\', startTime / 1000, \'unixepoch\', \'localtime\') != strftime(\'%d-%m-%Y\', date(\'now\', \'localtime\')) 
            AND strftime(\'%d-%m-%Y\', endTime / 1000, \'unixepoch\', \'localtime\') != strftime(\'%d-%m-%Y\', date(\'now\', \'localtime\')) 
            ORDER BY startTime DESC
            ''')).map<WorkedDuration>((workedDuration) => WorkedDuration.fromMap(workedDuration)).toList();
  }
}
