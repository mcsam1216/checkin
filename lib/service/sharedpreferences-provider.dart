import 'package:checkin/helper/constant.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesProvider {
  static SharedPreferences _sharedPreferences;

  static Future<void> initialize() async => _sharedPreferences = await SharedPreferences.getInstance();

  static void save(String key, dynamic value) {
    switch (value.runtimeType.toString()) {
      case 'String':
        {
          _sharedPreferences.setString(key, value);
          break;
        }
      case 'bool':
        {
          _sharedPreferences.setBool(key, value);
          break;
        }
      case 'int':
        {
          _sharedPreferences.setInt(key, value);
          break;
        }
      case 'Double':
        {
          _sharedPreferences.setDouble(key, value);
          break;
        }
      default:
        return;
    }
  }

  static dynamic get(String key) => _sharedPreferences.get(key);

  static Duration getRequiredDuration() {
    int requiredWorkingMinutes = _sharedPreferences.getInt(REQUIRED_MINUTES_DAILY);

    if (requiredWorkingMinutes != null)
      return new Duration(minutes: requiredWorkingMinutes);
    else
      return null;
  }

  static void remove(key) => _sharedPreferences.remove(key);

  static void removeAll() => _sharedPreferences.clear();
}
