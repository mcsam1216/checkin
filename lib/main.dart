import 'dart:ui';

import 'package:checkin/helper/constant.dart';
import 'package:checkin/helper/styles.dart';
import 'package:checkin/page/history.dart';
import 'package:checkin/page/home.dart';
import 'package:checkin/page/setting.dart';
import 'package:checkin/page/splash.dart';
import 'package:checkin/service/background-main.dart';
import 'package:checkin/service/sharedpreferences-provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';

import 'bloc/bloc-provider.dart';
import 'bloc/worked-duration-bloc.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  MethodChannel channel = MethodChannel('com.mcsam.checkin/background');
  CallbackHandle callbackHandle = PluginUtilities.getCallbackHandle(streamConnectivity);
  await channel.invokeMethod<void>('background', callbackHandle.toRawHandle());

  await SharedPreferencesProvider.initialize();
  runApp(App());
}

class App extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: mainTheme,
      home: Splash(),
    );
  }
}

class Main extends StatefulWidget {
  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {
  int _selectedIndex = 0;
  bool _shouldShowNavBar = true;

  ScrollController _homeScrollController;
  ScrollController _historyScrollController;
  ScrollController _settingScrollController;

  void _onItemTapped(int index) {
    setState(() => _selectedIndex = index);
  }

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarColor(Colors.transparent);
    FlutterStatusbarcolor.setStatusBarWhiteForeground(_selectedIndex == 0);

    Widget bottomNavBar = BottomNavigationBar(
      currentIndex: _selectedIndex,
      onTap: _onItemTapped,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('Home'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.history),
          title: Text('History'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.settings),
          title: Text('Setting'),
        )
      ],
    );

    return Scaffold(
      body: BlocProvider(
        bloc: WorkedDurationBloc(),
        child: IndexedStack(
          index: _selectedIndex,
          children: <Widget>[
            Home(),
            History(),
            Setting(),
          ],
        ),
      ),
      bottomNavigationBar: AnimatedContainer(
          duration: Duration(milliseconds: 500),
          height: _shouldShowNavBar ? NAVBAR_HEIGHT : 0.0,
          child: bottomNavBar),
    );
  }
}
