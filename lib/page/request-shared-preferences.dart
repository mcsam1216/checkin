import 'package:checkin/helper/constant.dart';
import 'package:checkin/helper/datetime.dart';
import 'package:checkin/service/sharedpreferences-provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_duration_picker/flutter_duration_picker.dart';
import 'package:flutter_statusbarcolor/flutter_statusbarcolor.dart';
import 'package:wifi/wifi.dart';

class RequestPreferences extends StatefulWidget {
  @override
  _RequestPreferencesState createState() => _RequestPreferencesState();
}

class _RequestPreferencesState extends State<RequestPreferences> with WidgetsBindingObserver {
  String _selectedWifi;
  Duration _requiredDuration;
  List<String> _wifiList;

  Future<void> _getWifiList() async {
    List<WifiResult> wifi_list = await Wifi.list('');
    List<String> result = wifi_list.map((wifi) => wifi.ssid.toString()).where((wifi) => wifi != '').toList().toSet().toList();

    setState(() => _wifiList = result);
  }

  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _getWifiList();
    }
  }

  Future<void> _openDurationPicker() async {

    Duration result = await showDurationPicker(
      context: context,
      initialTime: new Duration(hours: 8),
      boxDecoration: BoxDecoration(borderRadius: BorderRadius.circular(15.0), color: Colors.white),
    );

    if (result != null) setState(() => _requiredDuration = result);
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    _getWifiList();
  }


  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  @override
  Widget build(BuildContext context) {
    FlutterStatusbarcolor.setStatusBarWhiteForeground(false);

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: MediaQuery.of(context).padding.top),
              padding: EdgeInsets.symmetric(horizontal: 64.0),
              child: Image(
                image: AssetImage('assets/images/preferences.png'),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 35.0, vertical: 20.0),
                      child: Text(
                        'How long do you need to work per day?',
                        style: Theme.of(context).textTheme.title.merge(TextStyle(fontSize: 28.0)),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    InkWell(
                      onTap: _openDurationPicker,
                      child: Container(
                        width: MediaQuery.of(context).size.width * 0.5,
                          padding: EdgeInsets.fromLTRB(10.0, 15.0, 0, 15.0),
                          decoration: BoxDecoration(
                            border: Border.all(color: Theme.of(context).dividerColor),
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          child: Text(_requiredDuration != null ? getReadableHourMinute(_requiredDuration) : 'Select duration',),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 25.0,
                ),
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Text(
                        'Choose desired wi-fi that will start the timer when connected.',
                        style: Theme.of(context).textTheme.title.merge(TextStyle(fontSize: 26.0)),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.5,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        border: Border.all(color: Colors.grey, style: BorderStyle.solid, width: 0.80),
                      ),
                      padding: EdgeInsets.symmetric(horizontal: 10.0),
                      child: DropdownButtonFormField(
                        hint: Text('Select Wi-FI'),
                        value: _selectedWifi,
                        icon: Icon(Icons.arrow_drop_down),
                        decoration: InputDecoration.collapsed(hintText: ''),
                        validator: (value) {
                          if (value.isEmpty) return 'Please enter a value';
                          return null;
                        },
                        items: _wifiList?.map<DropdownMenuItem<String>>((String wifi) {
                          return DropdownMenuItem<String>(
                              value: wifi,
                              child: Text(
                                wifi,
                                style: Theme.of(context).textTheme.body1.merge(TextStyle(fontSize: 12.0)),
                              ));
                        })?.toList(),
                        onChanged: (val) {
                          setState(() => _selectedWifi = val);
                        },
                      ),
                    ),
                  ],
                )
              ],
            ),
            Container(
              alignment: Alignment.bottomCenter,
              child: MaterialButton(
                elevation: 0.0,
                minWidth: MediaQuery.of(context).size.width * 0.8,
                height: 28.0,
                textColor: Colors.white,
                color: Theme.of(context).primaryColor,
                highlightColor: Theme.of(context).accentColor,
                disabledColor: Colors.grey,
                disabledTextColor: Colors.grey[300],
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25.0)),
                onPressed: (_selectedWifi == null || _requiredDuration == null)
                    ? null
                    : () {
                        SharedPreferencesProvider.save(DESIRE_NETWORK, _selectedWifi);
                        SharedPreferencesProvider.save(REQUIRED_MINUTES_DAILY, _requiredDuration.inMinutes);
                        Navigator.of(context).pop(true);
                      },
                padding: EdgeInsets.symmetric(vertical: 15.0, horizontal: 25.0),
                child: Text(
                  'Let\'s Get Started',
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
