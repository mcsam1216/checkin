import 'package:checkin/widget/refresh.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';

import '../main.dart';

class Splash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
        seconds: 1,
        navigateAfterSeconds: new Refresh(child: Main(),),
        title: new Text('Checkin',
          style: Theme.of(context).textTheme.title.copyWith(color: Colors.white),),
        image: new Image(image: AssetImage('assets/images/logo.png'),),
        backgroundColor: Theme.of(context).backgroundColor,
        photoSize: 50.0,
    );
  }
}
