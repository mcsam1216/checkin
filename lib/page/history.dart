import 'package:checkin/bloc/bloc-provider.dart';
import 'package:checkin/bloc/worked-duration-bloc.dart';
import 'package:checkin/helper/data-factory.dart';
import 'package:checkin/helper/datetime.dart';
import 'package:checkin/model/worked-duration.dart';
import 'package:checkin/widget/common.dart';
import 'package:checkin/widget/date-picker.dart';
import 'package:checkin/widget/sliver-scroll-view.dart';
import 'package:checkin/widget/stream-builder.dart';
import 'package:checkin/widget/weekly-item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class History extends StatefulWidget {
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> with WidgetsBindingObserver {
  WorkedDurationBloc _workedDurationBloc;
  
  Widget _streamWidget(snapshot) {
    List<WorkedDuration> workedDurations = snapshot.data;
    List<WorkedDurationByWeek> workedDurationsByWeek = groupByWeek(workedDurations);

    Function itemBuilder = (BuildContext context, int index) {
      WorkedDurationByWeek workedDurationByWeek = workedDurationsByWeek[index];

      return DurationItemByWeek(
        item: workedDurationByWeek,
        bloc: _workedDurationBloc,
      );
    };

    return SliverList(
      delegate: SliverChildBuilderDelegate(itemBuilder, childCount: workedDurationsByWeek.length),
    );
  }

  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed) {
      _adjustDate();
      setState(() {});
    }
  }

  void _setStartDate(datePicker) {
    setState(() => WorkedDurationBloc.startDate = datePicker);
    _workedDurationBloc.sinkWorkedDurationType.add(WorkedDurationBloc.NOT_TODAY);
  }

  void _setEndDate(datePicker) {
    setState(() => WorkedDurationBloc.endDate = datePicker);
    _workedDurationBloc.sinkWorkedDurationType.add(WorkedDurationBloc.NOT_TODAY);
  }

  void _adjustDate() {
    WorkedDurationBloc.startDate = getFirstDateOfWeek(DateTime.now().subtract(Duration(days: 1)));
    WorkedDurationBloc.endDate = DateTime.now().subtract(Duration(days: 1));
  }

  @override
  Widget build(BuildContext context) {
    return CheckinSliverScrollView(
      children: <Widget>[
        SliverToBoxAdapter(
          child: TextTitle(title: 'History'),
        ),
        SliverToBoxAdapter(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 25.0),
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Flexible(
                  flex: 3,
                  child: CheckinDatePicker(
                    context: context,
                    datePicked: _setStartDate,
                    dateTime: WorkedDurationBloc.startDate,
                  ),
                ),
                Flexible(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: Icon(Icons.arrow_right, color: Theme.of(context).dividerColor,),
                  ),
                ),
                Flexible(
                  flex: 3,
                  child: CheckinDatePicker(
                    context: context,
                    datePicked: _setEndDate,
                    dateTime: WorkedDurationBloc.endDate,
                  ),
                )
              ],
            ),
          ),
        ),
        CheckinStreamBuilder(
          stream: _workedDurationBloc.streamNotTodayDuration,
          streamWidget: _streamWidget,
        ),
        SliverPadding(
          padding: EdgeInsets.symmetric(vertical: 35.0),
        )
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    _workedDurationBloc = BlocProvider.of<WorkedDurationBloc>(context);
    WidgetsBinding.instance.addObserver(this);

    _adjustDate();
    _workedDurationBloc.streamSharedPreferences.listen((shouldConfigure) {
      if(!shouldConfigure) _workedDurationBloc.sinkWorkedDurationType.add(WorkedDurationBloc.NOT_TODAY);
    });
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }
}
