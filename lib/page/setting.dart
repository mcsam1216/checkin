import 'dart:async';
import 'dart:ui';

import 'package:checkin/bloc/bloc-provider.dart';
import 'package:checkin/bloc/worked-duration-bloc.dart';
import 'package:checkin/helper/constant.dart';
import 'package:checkin/helper/datetime.dart';
import 'package:checkin/page/request-shared-preferences.dart';
import 'package:checkin/service/sharedpreferences-provider.dart';
import 'package:checkin/widget/common.dart';
import 'package:checkin/widget/sliver-scroll-view.dart';
import 'package:checkin/widget/snackbar.dart';
import 'package:checkin/widget/wifi-list.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_duration_picker/flutter_duration_picker.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';

class Setting extends StatefulWidget {
  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> with WidgetsBindingObserver {
  WorkedDurationBloc _workedDurationBloc;
  StreamSubscription _networkSubscription;
  Connectivity _connectivity;
  bool _isRunning = false;
  bool _permissionGranted = false;
  bool _shouldConfigure = true;

  void _checkSharedPreferences() {
    const List<String> requiredSharedPreferences = [DESIRE_NETWORK, REQUIRED_MINUTES_DAILY, WORKING_DAYS];
    bool _shouldSetup = requiredSharedPreferences.any((sp) => SharedPreferencesProvider.get(sp) == null);

    if(_shouldSetup) _showRequestPreferences();
    else _workedDurationBloc.sinkSharedPreferences.add(_shouldSetup);
  }

  void _showRequestPreferences() async {
    if(!_permissionGranted) return;

    Route _createRequestPreferencesRoute() {
      return PageRouteBuilder(
          pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) => RequestPreferences(),
          transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
            var positionTween = Tween(begin: Offset(0.0, 1.0), end: Offset.zero).chain(CurveTween(curve: Curves.ease));

            return SlideTransition(
              position: animation.drive(positionTween),
              child: child,
            );
          });
    }

    dynamic result = await Navigator.of(context).push(_createRequestPreferencesRoute()) ?? false;
    if (result) _workedDurationBloc.sinkSharedPreferences.add(false);
  }

  Future<void> _subscribeNetworkListener() async {
    print('Subscribe to network listener');
    try {
      if(_networkSubscription != null) _networkSubscription.cancel();

      _connectivity = Connectivity();
      _networkSubscription = _connectivity.onConnectivityChanged.listen((connectivityType) async {
        String desiredSSID = SharedPreferencesProvider.get(DESIRE_NETWORK) ?? '';
        String wifiSSID = await _connectivity.getWifiName();

        if (connectivityType == ConnectivityResult.wifi && wifiSSID == desiredSSID && !_isRunning) {
          _workedDurationBloc.sinkIsRunning.add(true);
        }

        if ((connectivityType == ConnectivityResult.none && _isRunning) || wifiSSID != desiredSSID) {
          _workedDurationBloc.sinkIsRunning.add(false);
        }
      });
    } on Exception catch (e, stackTrace) {
      print(stackTrace);
      Scaffold.of(context).showSnackBar(const SnackBar(content: Text('Something went wrong. Please restart')));
      return;
    }
  }

  void _checkPermissionsGranted() async{
    PermissionStatus _permissionStatus =  await Permission.locationAlways.status;

    switch(_permissionStatus) {
      case PermissionStatus.permanentlyDenied:
      case PermissionStatus.restricted:
      case PermissionStatus.denied:{
        _showDialog();
        break;
      }
      case PermissionStatus.undetermined: {
        Permission.locationAlways.request();
        break;
      }
      case PermissionStatus.granted:
        _workedDurationBloc.sinkPermission.add(true);
        break;
    }
  }


  void _showDialog() {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(15.0))),
            title: Container(
              padding: EdgeInsets.all(25.0),
              child: Column(
                children: <Widget>[
                  Image(
                    image: AssetImage('assets/images/warning.png'),
                  ),
                  SizedBox(
                    height: 35.0,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.location_off,
                        color: Theme.of(context).accentColor,
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Text(
                        'We need permission',
                        style: TextStyle(fontSize: 18.0, color: Colors.black45),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            content: Text(
              'We need location to be allow all the time (not only while in use) to keep track of wifi ID you connected/disconnected. Please go to \'Permission\' -> \'Location\' -> \'Allow all the time\'',
              style: TextStyle(fontSize: 16.0, color: Colors.black38),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Setting'),
                onPressed: () {
                  Navigator.of(context).pop();
                  openAppSettings();
                },
              )
            ],
          );
        });
  }

  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed) {
      _checkRequiredStatus(shouldCheckSharedPreferences: false);
    }
  }

  void _checkRequiredStatus({shouldCheckSharedPreferences: true}) {
    if(!_permissionGranted) {
      _checkPermissionsGranted();
      return;
    }

    if(_shouldConfigure && shouldCheckSharedPreferences) {
      _checkSharedPreferences();
      return;
    }

    _subscribeNetworkListener();
  }

  Future<void> _openDurationPicker() async {
    Duration requiredDuration = getDailyRequiredDuration();

    Duration result = await showDurationPicker(
      context: context,
      initialTime: requiredDuration,
      boxDecoration: BoxDecoration(borderRadius: BorderRadius.circular(15.0), color: Colors.white),
    );

    if (result != null) {
      SharedPreferencesProvider.save(REQUIRED_MINUTES_DAILY, result.inMinutes);
      _workedDurationBloc.sinkSharedPreferences.add(true);
      showSnackBar(context, 'Updated Successfully');
    }
  }

  Future<void> _openWifiPicker() async {
    dynamic result = await showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return WifiListDialog();
        });

    if (result != null) {
      SharedPreferencesProvider.save(DESIRE_NETWORK, result);
      _workedDurationBloc.sinkSharedPreferences.add(true);
      showSnackBar(context, 'Updated Successfully');
    }
  }

  Future<void> _openDaysPicker() async {
    int result = await showDialog(
      context: context,
      builder: (BuildContext context) {
        return NumberPickerDialog.integer(minValue: 1, maxValue: 7, initialIntegerValue: SharedPreferencesProvider.get(WORKING_DAYS), dialogShape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(15.0))),);
      }
    );

    if(result != null) {
      SharedPreferencesProvider.save(WORKING_DAYS, result);
      _workedDurationBloc.sinkSharedPreferences.add(true);
      showSnackBar(context, 'Updated Successfully');
    }
  }

  void _openBuyMeACoffee() async {
    if(await canLaunch(BUY_ME_A_COFFEE)) await launch(BUY_ME_A_COFFEE);
    else throw 'Could not load url';
  }

  @override
  Widget build(BuildContext context) {
    return CheckinSliverScrollView(
      children: <Widget>[
        SliverToBoxAdapter(
          child: TextTitle(title: 'Settings'),
        ),
        SettingRow(
          icon: Icons.access_time,
          title: 'Daily Working Minutes',
          value: _shouldConfigure ? '-' : '${getDailyRequiredDuration().inHours}h ${getDailyRequiredDuration().inMinutes.remainder(60)}m',
          onTap: _openDurationPicker,
        ),
        SettingRow(
          icon: Icons.wifi,
          title: 'Wi-Fi Point',
          value: _shouldConfigure ? '-' : '${SharedPreferencesProvider.get(DESIRE_NETWORK)}',
          onTap: _openWifiPicker,
        ),
        SettingRow(
          icon: Icons.work,
          title: 'Days of Working',
          value: '${SharedPreferencesProvider.get(WORKING_DAYS)}',
          onTap: _openDaysPicker,
        ),
        SliverToBoxAdapter(
          child: Container(
            height: 25.0,
          ),
        ),
        SliverToBoxAdapter(
          child: TextTitle(title: 'Credit'),
        ),
        SettingRow(
          icon: Icons.local_bar,
          title: 'Like my work?',
          value: 'Buy me a coffee',
          onTap: _openBuyMeACoffee,
        ),
      ],
    );
  }

  @override
  void initState() {
    super.initState();
    _workedDurationBloc = BlocProvider.of<WorkedDurationBloc>(context);
    WidgetsBinding.instance.addObserver(this);

    SharedPreferencesProvider.save(WORKING_DAYS, 5);

    _workedDurationBloc.streamIsRunning.listen((isRunning) => _isRunning = isRunning);
    _workedDurationBloc.checkIsRunning();
    _workedDurationBloc.streamPermission.listen((permissionGranted) {
      _permissionGranted = permissionGranted;
      _checkRequiredStatus();
    });
    _workedDurationBloc.streamSharedPreferences.listen((shouldConfigure) {
      setState(() => _shouldConfigure = shouldConfigure);
      _checkRequiredStatus();
    });
    _checkRequiredStatus();
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
    if (_networkSubscription != null) _networkSubscription.cancel();
  }
}

class SettingRow extends StatefulWidget {
  IconData icon;
  String title;
  String value;
  Function onTap;

  SettingRow({
    @required this.icon,
    @required this.title,
    @required this.value,
    this.onTap,
  });

  @override
  _SettingRowState createState() => _SettingRowState();
}

class _SettingRowState extends State<SettingRow> {
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: InkWell(
        onTap: widget.onTap != null ? widget.onTap : () {},
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 35.0, horizontal: 25.0),
          decoration: BoxDecoration(
            border: Border(bottom: BorderSide(width: 0.5, color: Theme.of(context).dividerColor)),
          ),
          child: Row(
            children: <Widget>[
              Icon(
                widget.icon,
                color: Colors.orange,
                size: 40.0,
              ),
              Container(
                margin: EdgeInsets.only(left: 15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.title,
                      style: Theme.of(context).textTheme.subhead,
                    ),
                    SizedBox(
                      height: 4.0,
                    ),
                    Text(
                      widget.value,
                      style: TextStyle(fontSize: 16.0),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
