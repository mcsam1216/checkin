import 'dart:async';

import 'package:checkin/bloc/bloc-provider.dart';
import 'package:checkin/bloc/worked-duration-bloc.dart';
import 'package:checkin/helper/datetime.dart';
import 'package:checkin/helper/progress.dart';
import 'package:checkin/model/worked-duration.dart';
import 'package:checkin/widget/card.dart';
import 'package:checkin/widget/datetime-progress.dart';
import 'package:checkin/widget/loader.dart';
import 'package:checkin/widget/refresh.dart';
import 'package:checkin/widget/sliver-scroll-view.dart';
import 'package:checkin/widget/stream-builder.dart';
import 'package:checkin/widget/today-item.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with WidgetsBindingObserver {
  WorkedDurationBloc _workedDurationBloc;

  Duration _todayTotalWorkedDuration;
  Duration _thisWeekTotalWorkedDuration;
  Duration _dailyRequiredDuration;
  Duration _weeklyRequiredDuration;

  double _todayProgress;
  double _thisWeekProgress;

  int _todayPercentage;
  int _thisWeekPercentage;

  String _startTime;
  Timer _realTimeSubscription;

  bool _shouldConfigure;
  bool _isRunning;

  void _startTimer() {
    print('Starting second periodic timer');
    if(_realTimeSubscription != null) _realTimeSubscription.cancel();
    _realTimeSubscription = Timer.periodic(Duration(seconds: 1), (Timer timer) {
      _workedDurationBloc.sinkWorkedDurationType.add(WorkedDurationBloc.TODAY);
      _workedDurationBloc.sinkWorkedDurationType.add(WorkedDurationBloc.THIS_WEEK);

      Timer _timer;
      DateTime _now = DateTime.now();
      if(_now.hour == 23 && _now.minute == 59 && _now.second == 59) {
        _workedDurationBloc.sinkIsRunning.add(false);

        _timer = Timer.periodic(Duration(seconds: 1), (i) {
          _workedDurationBloc.sinkIsRunning.add(true);
          _timer.cancel();
        });
      }
    });
  }

  void _listenStaticTodayWorkedDuration(List<WorkedDuration> todayWorkedDurations) {
    if (todayWorkedDurations.isEmpty) return;
    int _todayTotalWorkedSeconds = todayWorkedDurations.fold(0, (total, workingDuration) {
      total += workingDuration.endTime.difference(workingDuration.startTime).inSeconds;
      return total;
    });
    DateTime startTime = todayWorkedDurations.reduce((curr, next) => curr.startTime.isBefore(next.startTime) ? curr : next).startTime;
    setState((){
      _todayTotalWorkedDuration = Duration(seconds: _todayTotalWorkedSeconds);
      _startTime = formatDate(startTime, [HH, ':', nn, ':', ss]);
      _todayProgress = _todayTotalWorkedDuration.inMinutes / (_dailyRequiredDuration.inMinutes);
      _todayPercentage = (_todayProgress * 100).round();
    });
  }

  void _listenThisWeekWorkedDuration(List<WorkedDuration> thisWeekWorkedDuration) {
    if(thisWeekWorkedDuration.isEmpty) return;
    int thisWeekTotalWorkedSeconds = thisWeekWorkedDuration.fold(0, (total, workingDuration) {
      total += workingDuration.endTime.difference(workingDuration.startTime).inSeconds;
      return total;
    });
    setState(() {
      _thisWeekTotalWorkedDuration = Duration(seconds: thisWeekTotalWorkedSeconds);
      _thisWeekProgress = _thisWeekTotalWorkedDuration.inMinutes / _weeklyRequiredDuration.inMinutes;
      _thisWeekPercentage = (_thisWeekProgress * 100).round();
    });
  }

  void didChangeAppLifecycleState(AppLifecycleState state) async {
    if (state == AppLifecycleState.resumed && !_shouldConfigure)
      new Future.delayed(Duration.zero,() =>
        Refresh.restartApp(context)
      );
  }

  void initialize(){
    if(_shouldConfigure) return;

    _dailyRequiredDuration = getDailyRequiredDuration();
    _weeklyRequiredDuration = getWeeklyRequiredDuration();
    if(_isRunning) _startTimer();
    else {
      if(_realTimeSubscription != null) _realTimeSubscription.cancel();
      _workedDurationBloc.sinkWorkedDurationType.add(
          WorkedDurationBloc.TODAY);
      _workedDurationBloc.sinkWorkedDurationType.add(WorkedDurationBloc.THIS_WEEK);
    }
  }

  @override
  void initState() {
    super.initState();
    _workedDurationBloc = BlocProvider.of<WorkedDurationBloc>(context);
    WidgetsBinding.instance.addObserver(this);

    _todayTotalWorkedDuration = Duration(seconds: 0);
    _thisWeekTotalWorkedDuration = Duration(seconds: 0);
    _todayProgress = 0.0;
    _todayPercentage = 0;
    _thisWeekProgress = 0.0;
    _thisWeekPercentage = 0;
    _startTime = '00:00:00';
    _dailyRequiredDuration = Duration(seconds: 0);
    _weeklyRequiredDuration = Duration(seconds: 0);
    _shouldConfigure = true;
    _isRunning = false;

    _workedDurationBloc.streamTodayDuration.listen(_listenStaticTodayWorkedDuration);
    _workedDurationBloc.streamThisWeekDuration.listen(_listenThisWeekWorkedDuration);
    _workedDurationBloc.streamIsRunning.listen((isRunning) {
      _isRunning = isRunning;
      initialize();
    });
    _workedDurationBloc.streamSharedPreferences.listen((shouldConfigure) {
      _shouldConfigure = shouldConfigure;
      initialize();
    });
    _workedDurationBloc.checkIsRunning();
  }

  @override
  void dispose() {
    super.dispose();
    if (_realTimeSubscription != null) _realTimeSubscription.cancel();
  }

  @override
  Widget build(BuildContext context) {

    // ignore: non_constant_identifier_names
    Widget Time(String time, String desc) => Container(
      margin: EdgeInsets.symmetric(horizontal: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Text(time, style: Theme.of(context).textTheme.title.copyWith(fontSize: 56.0, color: Colors.white),),
          Container(margin: EdgeInsets.only(right: 8.0),child: Text(desc, style: Theme.of(context).textTheme.title.copyWith(fontSize: 12.0, color: Colors.white),)),
        ],
      ),
    );

    List<Widget> cardList = [
      CheckinCard(
        margin: EdgeInsets.only(bottom: 15.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Text('Daily Progress', style: Theme.of(context).textTheme.subtitle),
            ),
            Expanded(
              child: Row(
                children: <Widget>[
                  Container(
                    width: 130.0,
                    height: 130.0,
                    child: LiquidCircularProgressIndicator(
                      value: _todayProgress,
                      valueColor: AlwaysStoppedAnimation(getProgressColor(_todayPercentage)),
                      backgroundColor: Colors.white,
                      borderColor: getProgressColor(_todayPercentage, swatch: 300),
                      borderWidth: 0.5,
                      direction: Axis.vertical,
                      center: _todayPercentage > 100 ? Icon(Icons.check, color: Colors.white,) : Text('$_todayPercentage%', style: TextStyle(color: _todayPercentage > 60 ? Colors.white : getProgressColor(_todayPercentage)),),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Expanded(
                          flex: 2,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text('Start time'),
                              Text(_startTime, style: Theme.of(context).textTheme.subtitle.copyWith(fontSize: 22.0),)
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: DateTimeProgress(
                            workedDuration: _todayTotalWorkedDuration,
                            requiredWorkedDuration: _dailyRequiredDuration,
                            showMinutes: false,
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      CheckinCard(
        margin: EdgeInsets.only(bottom: 15.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(flex: 2, child: Text('Weekly Progress', style: Theme.of(context).textTheme.subtitle,)),
            Expanded(
              flex: 1,
              child: LiquidLinearProgressIndicator(
                value: _thisWeekProgress,
                valueColor: AlwaysStoppedAnimation(getProgressColor(_thisWeekPercentage)),
                backgroundColor: Colors.white,
                direction: Axis.horizontal,
                borderRadius: 15.0,
                center: Text('$_thisWeekPercentage%', style: TextStyle(color: _thisWeekPercentage > 60 ? Colors.white : Colors.black54),),
                borderColor: getProgressColor(_thisWeekPercentage, swatch: 300),
                borderWidth: 0.5,
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(top: 15.0),
                child: DateTimeProgress(
                  workedDuration: _thisWeekTotalWorkedDuration,
                  requiredWorkedDuration: _weeklyRequiredDuration,
                  showMinutes: true,
                ),
              ),
            )
          ],
        ),
      )
    ];

    Widget _streamWidget(snapshot) {
      List<WorkedDuration> workedDurations = snapshot.data;
      Function itemBuilder = (BuildContext context, int index) {
        WorkedDuration workedDuration = workedDurations[index];

        return TodayItem(
          startTime: workedDuration.startTime,
          endTime: workedDuration.endTime,
        );
      };

      return SliverList(
        delegate: SliverChildBuilderDelegate(itemBuilder, childCount: workedDurations.length),
      );
    }

    return RefreshIndicator(
      onRefresh: () => Future.delayed(Duration(milliseconds: 500), () => Refresh.restartApp(context)),
      child: CheckinSliverScrollView(
        ignoreStatusBar: true,
        children: <Widget>[
          SliverToBoxAdapter(
            child: Container(
              height: 550,
              child: Stack(
                children: <Widget>[
                  FractionallySizedBox(
                    heightFactor: 0.75,
                    widthFactor: 1,
                    child: Container(
                      decoration: BoxDecoration(borderRadius: BorderRadius.vertical(bottom: new Radius.elliptical(MediaQuery.of(context).size.width, 100.0),),
                        color: Theme.of(context).backgroundColor,
                      ),
                      child: Padding(
                        padding: EdgeInsets.only(top: 130),
                        child: Align(
                        alignment: Alignment.topCenter,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text('Today', style: Theme.of(context).textTheme.title.copyWith(color: Colors.white),),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Time(getHours(_todayTotalWorkedDuration), 'HRS'),
                                Text(':',style: Theme.of(context).textTheme.subtitle.copyWith(color: Colors.white),),
                                Time(getMinutes(_todayTotalWorkedDuration), 'MIN'),
                                Text(':',style: Theme.of(context).textTheme.subtitle.copyWith(color: Colors.white),),
                                Time(getSeconds(_todayTotalWorkedDuration), 'SEC'),
                              ],
                            )
                        ],
                            ),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(bottom: 25.0),
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                        height: 220.0,
                        child: Swiper(
                          itemBuilder: (BuildContext context, int index) {
                            return cardList[index];
                          },
                          itemCount: cardList.length,
                          viewportFraction: 0.8,
                          scale: 0.75,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                      right: _isRunning ? null : -40,
                      left: _isRunning ? -15 : null,
                      top: _isRunning ? 10 : 0,
                      child: Image.asset(_isRunning ? 'assets/images/active.png' : 'assets/images/inactive.png', fit: BoxFit.fitWidth, height: 140, alignment: Alignment.topRight,)
                  ),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.fromLTRB(25.0, 0, 25.0, 25.0),
              child: Center(child: Text('Today\'s Log', style: Theme.of(context).textTheme.subtitle,)),
            ),
          ),
          CheckinStreamBuilder(
            stream: _workedDurationBloc.streamTodayDuration,
            streamWidget: _streamWidget,
            loader: CheckinLoader(
              child: Column(
                children: List.generate(5, (i) => TodayItem()),
              ),
            ),
          )
        ],
      ),
    );
  }
}
