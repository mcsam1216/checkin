import 'package:flutter/cupertino.dart';

class WorkedDuration {
  int id;
  DateTime startTime;
  DateTime endTime;

  static const String TABLE_NAME = 'WorkedDuration';

  WorkedDuration({this.id, @required this.startTime, @required this.endTime});

  factory WorkedDuration.fromMap(Map<String, dynamic> map) {
    return WorkedDuration(
        id: map['id'],
        startTime: DateTime.fromMillisecondsSinceEpoch(map['startTime']),
        endTime: map['endTime'] == 0 ? DateTime.now() : DateTime.fromMillisecondsSinceEpoch(map['endTime']));
  }

  Map<String, dynamic> toPrint() => {
        "id": id,
        "startTime": startTime,
        "endTime": endTime,
      };

  Map<String, dynamic> toMap() => {
        "id": id,
        "startTime": startTime.millisecondsSinceEpoch,
        "endTime": endTime == null ? 0 : endTime.millisecondsSinceEpoch,
      };
}

class WorkedDurationByDay {
  DateTime startTime;
  DateTime endTime;
  Duration totalWorkedDuration;

  WorkedDurationByDay({this.startTime, this.endTime, this.totalWorkedDuration});
}

class WorkedDurationByWeek {
  DateTime startDate;
  DateTime endDate;
  Duration totalWorkedDuration;
  List<WorkedDurationByDay> dailyWorkedDuration;

  WorkedDurationByWeek({this.startDate, this.endDate, this.totalWorkedDuration, this.dailyWorkedDuration});

  Map<String, dynamic> toPrint() => {
    "startDate": startDate,
    "endDate": endDate,
    "totalWorkedDuration": totalWorkedDuration,
    "dailyWorkedDuration": dailyWorkedDuration.length
  };
}
