package com.mcsam.checkin;

import io.flutter.app.FlutterApplication;

public class MainApplication extends FlutterApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(
                LifecycleDetector.activityLifecycleCallbacks);
    }
}
