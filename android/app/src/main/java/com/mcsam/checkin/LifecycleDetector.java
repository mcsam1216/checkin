package com.mcsam.checkin;


import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

class LifecycleDetector {
    interface Listener {
        void onFlutterActivityCreated();
        void onFlutterActivityDestroyed();
    }

    static Listener listener = null;


    static Application.ActivityLifecycleCallbacks activityLifecycleCallbacks = new Application.ActivityLifecycleCallbacks() {

        @Override
        public void onActivityCreated(Activity activity, Bundle bundle) {
            if(activity instanceof MainActivity) if(listener != null) listener.onFlutterActivityCreated();

        }

        @Override
        public void onActivityStarted(Activity activity) {}

        @Override
        public void onActivityResumed(Activity activity) {}

        @Override
        public void onActivityPaused(Activity activity) {}

        @Override
        public void onActivityStopped(Activity activity) {}

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {}

        @Override
        public void onActivityDestroyed(Activity activity) {
            if(activity instanceof MainActivity)  if(listener != null) listener.onFlutterActivityDestroyed();
        }
    };
}
