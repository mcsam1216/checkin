package com.mcsam.checkin;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity implements MethodChannel.MethodCallHandler {
  static final String TAG = "checkin";
  static final String CHANNEL = "com.mcsam.checkin/background";
  static final String SHARED_PREFERENCES_NAME = "com.mcsam.BackgroundService";

  static final String KEY_CALLBACK_HANDLE = "callbackHandle";

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);

    new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(this::onMethodCall);
    registerService();
  }

  @Override
  public void onMethodCall(MethodCall call, MethodChannel.Result result) {
    try {
      if (call.method.equals("background")) {
        Long callbackHandle = (Long) call.arguments;

        SharedPreferences prefs = getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        prefs.edit().putLong(KEY_CALLBACK_HANDLE, callbackHandle).apply();
        result.success(null);
      } else {
        result.error(null, "App not connected to service", null);
      }

    } catch (Exception e) {
      result.error(null, e.getMessage(), null);
    }
  }

  void registerService() {
    if(!isServiceRunningInForeground(getApplicationContext(), CheckinService.class)) {
      Intent serviceIntent = new Intent(getApplicationContext(), CheckinService.class);

      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        startForegroundService(serviceIntent);
      } else {
        startService(serviceIntent);
      }
    }
  }

  private boolean isServiceRunningInForeground(Context context, Class<?> serviceClass) {
    ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
      if (serviceClass.getName().equals(service.service.getClassName())) {
        return service.foreground;
      }
    }
    return false;
  }
}
