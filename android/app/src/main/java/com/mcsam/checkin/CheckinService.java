package com.mcsam.checkin;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.IBinder;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import java.util.HashMap;
import java.util.Map;

import io.flutter.Log;
import io.flutter.plugins.GeneratedPluginRegistrant;
import io.flutter.view.FlutterCallbackInformation;
import io.flutter.view.FlutterMain;
import io.flutter.view.FlutterNativeView;
import io.flutter.view.FlutterRunArguments;

public class CheckinService extends Service implements LifecycleDetector.Listener {
    FlutterNativeView flutterNativeView = null;
    static final String CHANNEL_ID = "ForegroundServiceChannel";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle extras = intent.getExtras();

        if(extras == null) {
        	Log.i("Service", "Started background service from app");
		} else {
        	Log.i("Service", "Started background service after restart");
        	Boolean restarted = (Boolean) extras.get("restarted");

        	if(restarted) startFlutterNativeView();
		}

    	return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Checkin service")
                .setContentText("Checkin service running. Tap to open Checkin.")
                .setSmallIcon(R.drawable.sloth)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);

        LifecycleDetector.listener = this;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        if(wifiReceiver!=null) getApplicationContext().unregisterReceiver(wifiReceiver);
//        Log.i(MainActivity.TAG, "Wifi Receiver Unregistered");

        LifecycleDetector.listener = null;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    @Override
    public void onFlutterActivityCreated() {
        stopFlutterNativeView();
    }

    @Override
    public void onFlutterActivityDestroyed() {
        startFlutterNativeView();
    }

    private void startFlutterNativeView() {
        Log.i("BackgroundService", "Starting FlutterNativeView");
        FlutterMain.startInitialization(this);
        FlutterMain.ensureInitializationComplete(this, null);

        flutterNativeView = new FlutterNativeView(this, true);
        GeneratedPluginRegistrant.registerWith(flutterNativeView.getPluginRegistry());

        FlutterCallbackInformation callbackInformation = getCallbackInformation();

        FlutterRunArguments args = new FlutterRunArguments();
        args.bundlePath = FlutterMain.findAppBundlePath();
        args.entrypoint = callbackInformation.callbackName;
        args.libraryPath = callbackInformation.callbackLibraryPath;

        flutterNativeView.runFromBundle(args);
    }

    private FlutterCallbackInformation getCallbackInformation() {
        SharedPreferences prefs = getSharedPreferences(MainActivity.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        Long callbackRawHandle = prefs.getLong(MainActivity.KEY_CALLBACK_HANDLE, -1);
        if (callbackRawHandle != -1L)
            return FlutterCallbackInformation.lookupCallbackInformation(callbackRawHandle);
        return null;
    }

    private void stopFlutterNativeView() {
        Log.i(MainActivity.TAG, "Stopping FlutterNativeView");
        if(flutterNativeView != null) {
            flutterNativeView.destroy();
            flutterNativeView = null;
        }
    }
}
